#!/usr/bin/python3 -OO

"""
Example program
"""
import types
from NTIEval import CNTIEval
from NTIEventUnitTS import CNTIEventUnitTS
from NTIMotif import CNTIMotif

"""
The Measurement API would receive a measurement, look up the user session,
and iterate through the motifs.  The following code ensures that for 
each motif only one evaluation would need to occur.  This ensures that even
if there were 5 or 10 motifs per course only 5 or 10 evals per measurement would
be required, keeping the system scalable.

The first that triggered would get the alternate content displayed.

"""
# We'll build a motif directly.  In practice, we'll have sub-classed motifs where
# we load variables for the course from the DB.
motif = CNTIMotif(1)

# Create a few CNTIEventUnitTS instances for the motif
# If we spend more than 100 seconds on Unit ID 5 . . .
eEvalInstance = CNTIEventUnitTS(1, False, None, 5, 100)
motif.addEval(eEvalInstance)
# and we then spend more than 100 seconds on Unit ID 6 then trigger alternate content
eEvalInstance2 = CNTIEventUnitTS(1, True, "https://api.test.com/xxxxx", 6, 100)
motif.addEval(eEvalInstance2)

# Now create a few simple measurements
mAPIMeasurement5 = types.SimpleNamespace()
setattr(mAPIMeasurement5, 'unit_id', 5)
setattr(mAPIMeasurement5, 'unit_ts', 35)

mAPIMeasurement6 = types.SimpleNamespace()
setattr(mAPIMeasurement6, 'unit_id', 6)
setattr(mAPIMeasurement6, 'unit_ts', 35)

# In this case, we will advance to the next eval after 3 occurrences,
# but then this measurement should be ignored
for x in range(0, 7):
    triggered, alternateContent = motif.motifEval(mAPIMeasurement5)
    # The following should never happen.  This is here to prove it.
    if triggered:
        print("Triggered mAPImasurement 5 and alternate content", alternateContent)

# and then after 3 occurences this should trigger eInstance2 eval
for x in range(0, 3):
    triggered, alternateContent = motif.motifEval(mAPIMeasurement6)
    if triggered:
        print("Triggered mAPImasurement 6 and alternate content", alternateContent)

# Reset the Motif and do it again
motif.resetMotif()

# In this case, we will advance to the next eval after 3 occurrences,
# but then this measurement should be ignored
for x in range(0, 7):
    triggered, alternateContent = motif.motifEval(mAPIMeasurement5)
    # The following should never happen.  This is here to prove it.
    if triggered:
        print("Triggered mAPImasurement 5 and alternate content", alternateContent)

# and then after 3 occurences this should trigger eInstance2 eval
for x in range(0, 3):
    triggered, alternateContent = motif.motifEval(mAPIMeasurement6)
    if triggered:
        print("Triggered mAPImasurement 6 and alternate content after reset", alternateContent)
