"""
Base class for NTIEval instances
"""
class CNTIEval(object):
    """
    Initialization.
    Note that "triggerTarget" could be an array and the Eval could decide which to use
    based on it's result.  In that case, store the triggerTarget arg in a different
    variable and when ready set self.triggerTarget to the correct result
    """
    def __init__(self, id, isTriggerTarget, triggerTarget):
        self.id = id
        self.isTriggerTarget = isTriggerTarget
        self.triggerTarget = triggerTarget
        """
        Alternately, save a list of trigger targets and have the Eval function set the triggerTarget
        
        self.triggerTarget = someDefault, like triggerTarget[0]
        self.triggerTargetArr = triggerTarget
        """

    """
    Evaluate the measuremeant
    """
    def Eval(self, mAPIMeasurement):
        return False

    """
    Reset the eval
    """
    def Reset(self):
        return False