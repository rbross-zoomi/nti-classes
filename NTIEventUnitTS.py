"""
Eval class if time spent on a specific unit exceeds a threshold
Note that this class is a sub-class of CNTIEval
"""

from NTIEval import CNTIEval

class CNTIEventUnitTS(CNTIEval):
    """
    Initialization
    """
    def __init__(self, id, isTrigger, triggerTarget, unit_id, unit_threshold):
        CNTIEval.__init__(self, id, isTrigger, triggerTarget)
        self.totalTS = 0
        self.thresholdTS = unit_threshold
        self.unit_id = unit_id

    """
    Evaluate the measuremeant
    """
    def Eval(self, mAPIMeasurement):
        if mAPIMeasurement.unit_id == self.unit_id:
            self.totalTS += mAPIMeasurement.unit_ts
            return self.totalTS >= self.thresholdTS
        else:
            return False

    """
    Reset the eval
    """
    def Reset(self):
        self.totalTS = 0