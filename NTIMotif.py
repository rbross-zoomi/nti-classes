"""
Base class for NTIEval instances
"""
from NTIEval import CNTIEval

class CNTIMotif(object):
    """
    Initialization.
    """
    def __init__(self, id):
        self.id = id
        self.evalArr = []
        self.currentEvalIndex = 0

    """
    Add an eval to our list.  Returns False if the argument is not a subclass of CNTIEval 
    """
    def addEval(self, newEval):
        if not issubclass(newEval.__class__, CNTIEval):
            return False
        self.evalArr.append(newEval)
        return True

    """
    Reset the motif
    """
    def resetMotif(self):
        # Tell each eval to reset itself
        for cEval in self.evalArr:
            cEval.Reset()
        # Reset us to the first
        self.currentEvalIndex = 0

    """
    Perform the current evaluation
    Returns True, triggerTarget if we want alternate content
    This could be modified in conjunction with the CNTIEval class to allow skipping around the eval order
    """
    def motifEval(self, mAPIMeasurement):
        # Shortcut
        currentEval = self.evalArr[self.currentEvalIndex]
        # If the current eval evaluates to True . . .
        if currentEval.Eval(mAPIMeasurement):
            # If this is the trigger target
            if currentEval.isTriggerTarget:
                return True, currentEval.triggerTarget
            # If not the trigger target, advance to the next eval
            # If the last in the list is not the trigger target, this will be out of bounds
            self.currentEvalIndex += 1
        # No alternate content
        return False, None


